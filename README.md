# InstallOpenVPNServer

Installs and configures an OpenVPN server on a given Ubuntu 16.04 installtion in Amazon E.C.2. Once the script has been run the server will have a working OpenVPN server with a pre-generated OpenVPN client configuration file for download. This script can be trivially adapted to run on outside Amazon E.C.2. (see script contents). 

## Copyright

This project/script is licensed under the terms of the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode) by Conor Andrew Buckley (Copyright 2018). This project/script is based off '[How To Set Up an OpenVPN Server on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-16-04)' by [Justin Ellingwood](https://www.digitalocean.com/community/users/jellingwood) and as such contains some content from the original work. A copy of the licence is provided in the LICENSE file. 

## Usage Instructions

This script is intended to be used with '[makeEC2VPN](https://gitlab.com/conorab/makeEC2VPN)'. See the project page for more details. 
